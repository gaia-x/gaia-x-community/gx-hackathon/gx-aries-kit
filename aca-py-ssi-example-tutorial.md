# ACA-Py SSI example tutorial

# Setup and Installation

## Start a von network:
```bash
git clone https://github.com/bcgov/von-network
cd von-network
pip install -r server/requirements.txt
pip install python3-indy
./manage build
./manage start
```

### If you get this error 
- `docker-compose: error while loading shared libraries: libz.so.1: failed to map segment from shared object`
- Do this:
    - `sudo mount /tmp -o remount,exec`

## Install indy tails server
```bash
git clone https://github.com/bcgov/indy-tails-server.git
./indy-tails-server/docker/manage up
```
Copy the url `url`, e.g. `https://{random code}.ngrok.io` from logs you will need it later.
```
docker logs docker_ngrok-tails-server_1
```

## Create public dids on the indy ledger
- Create public `did` for issuer and verifier in the ledger browser running on the port`9000` using two random seeds which we will use later.
- ### For example
    - Issuer:
      - 0923456ERFDZSXCVTYUO9986OREDFBBB
    - Verifier:
      - 0923456ERFDZSXCVTYUO9986VERIFIER
    - Holder:
      - Holder does not require a public did creation
# Agent creation
**Note** You do not have create three agents to establish ssi, you can also use one agent for `issuer/verifier` and one for holder.

## Start agents with docker

### Issuer Agent creation
```bash
docker run --network=host bcgovimages/aries-cloudagent:py36-1.16-1_0.7.3 start \
    --label issuer \
    --auto-respond-messages \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label issuer.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --genesis-url http://localhost:9000/genesis \
    --wallet-type indy \
    -it http 0.0.0.0 8050 \
    -ot http \
    --admin 0.0.0.0 8051 \
    --admin-insecure-mode \
    --auto-ping-connection \
    --seed 0923456ERFDZSXCVTYUO9986OREDFBBB \
    -e http://localhost:8050 \
    --wallet-name issuer \
    --wallet-key issuer \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io}
```
### Verifier Agent creation
```bash
docker run --network=host bcgovimages/aries-cloudagent:py36-1.16-1_0.7.3 start \
    --label verifier \
    --auto-respond-messages \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label verifier.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --genesis-url http://localhost:9000/genesis \
    --wallet-type indy \
    -it http 0.0.0.0 8070 \
    -ot http --admin 0.0.0.0 8071 \
    --admin-insecure-mode \
    --auto-ping-connection \
    --seed 0923456ERFDZSXCVTYUO9986VERIFIER \
    -e http://localhost:8070 \
    --wallet-name verifier \
    --wallet-key verifier \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io}
```
### Holder Agent creation
```bash
docker run --network=host bcgovimages/aries-cloudagent:py36-1.16-1_0.7.3 start \
    -e http://localhost:8090 \
    --label holder \
    --auto-ping-connection \
    --auto-respond-messages \
    --inbound-transport http 0.0.0.0 8090 \
    --outbound-transport http \
    --admin 0.0.0.0 8091 \
    --admin-insecure-mode \
    --wallet-type indy \
    --preserve-exchange-records \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label holder.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --auto-store-credential \
    --debug-credentials \
    --genesis-url http://localhost:9000/genesis \
    --wallet-name holder \
    --wallet-key holder \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io}
```

## Start agents from source
### Install libindy
```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys CE7709D068DB5E88
sudo add-apt-repository "deb https://repo.sovrin.org/sdk/deb bionic master"
sudo apt-get update
sudo apt-get install -y libindy
```

### Install ACA-Py cli through pip
```bash
pip3 install aries-cloudagent
```
See if the installation was successful and also the available options:
```bash
aca-py start --help
```

### Install requirements for ACA-Py
- Install requirements for aca-py, from `requirements.txt` in this repository.

### Issuer agent creation
```bash
aca-py start \
    --label issuer \
    --auto-respond-messages \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label issuer.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --genesis-url http://localhost:9000/genesis \
    --wallet-type indy \
    -it http 0.0.0.0 8050 \
    -ot http \
    --admin 0.0.0.0 8051 \
    --admin-insecure-mode \
    --auto-ping-connection \
    --seed 0923456ERFDZSXCVTYUO9986OREDFBBB \
    -e http://localhost:8050 \
    --wallet-name issuer \
    --wallet-key issuer \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io}
```


### Verifier agent creation
```bash
aca-py start \
    --label verifier \
    --auto-respond-messages \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label verifier.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --genesis-url http://localhost:9000/genesis \
    --wallet-type indy \
    -it http 0.0.0.0 8070 \
    -ot http --admin 0.0.0.0 8071 \
    --admin-insecure-mode \
    --auto-ping-connection \
    --seed 0923456ERFDZSXCVTYUO9986VERIFIER \
    -e http://localhost:8070 \
    --wallet-name verifier \
    --wallet-key verifier \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io}
```

### Holder Agent creation
```bash
aca-py start \
    -e http://localhost:8090 \
    --label holder \
    --auto-ping-connection \
    --auto-respond-messages \
    --inbound-transport http 0.0.0.0 8090 \
    --outbound-transport http \
    --admin 0.0.0.0 8091 \
    --admin-insecure-mode \
    --wallet-type indy \
    --preserve-exchange-records \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label holder.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --auto-store-credential \
    --debug-credentials \
    --genesis-url http://localhost:9000/genesis \
    --wallet-name holder \
    --wallet-key holder \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io}
```
# ACA-Py API GUI SSI establishment

**NOTE**  For communication with ACA-Py API, you can use tool like [PostMan](https://postman.com).

# 1. Credential creation

## Establish connection between Agents
- Connect two agents by creating invitation at `issuer_admin_url POST/connections/create-invitation` the body of request should contain `{}`.
```json
{
    "connection_id": "03ed149e-bb04-47f3-978a-34301d959f4e", // we will need this id later in tutorial
    "invitation": {
        "@type": "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation",
        "@id": "e8f32994-266c-406f-97e8-49d42c483f7c",
        "recipientKeys": [
            "6MGvTLzmFLbVvVx1t7ZqvDTXnPSumELsey5y1zPzNBQU"
        ],
        "serviceEndpoint": "http://localhost:8050",
        "label": "issuer"
    },
    "invitation_url": "http://localhost:8050?c_i=eyJAdHlwZSI6ICJkaWQ6c292OkJ6Q2JzTlloTXJqSGlxWkRUVUFTSGc7c3BlYy9jb25uZWN0aW9ucy8xLjAvaW52aXRhdGlvbiIsICJAaWQiOiAiZThmMzI5OTQtMjY2Yy00MDZmLTk3ZTgtNDlkNDJjNDgzZjdjIiwgInJlY2lwaWVudEtleXMiOiBbIjZNR3ZUTHptRkxiVnZWeDF0N1pxdkRUWG5QU3VtRUxzZXk1eTF6UHpOQlFVIl0sICJzZXJ2aWNlRW5kcG9pbnQiOiAiaHR0cDovL2xvY2FsaG9zdDo4MDUwIiwgImxhYmVsIjogImlzc3VlciJ9"
}
```
- Save somewhere the `connection_id` from above we will use it later and paste the `invitation` field of the response to the holder into the  body of `holder_admin_url POST/connections/receive-invitation`.
```json
{
    "@type": "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation",
    "@id": "e8f32994-266c-406f-97e8-49d42c483f7c",
    "recipientKeys": [
        "6MGvTLzmFLbVvVx1t7ZqvDTXnPSumELsey5y1zPzNBQU"
    ],
    "serviceEndpoint": "http://localhost:8050",
    "label": "issuer"
}
```
- Now both agents should be connected successfully!
- In this case the connection was made between issuer and holder. Later in this tutorial we will need to also connected holder and verifier the same process.

## Claim the issuance
- Create an example schema by request at `issuer_admin_url POST/schemas`.
- This is an example schema:
```json
{
  "schema_name": "example_schema",
  "attributes":[
    "example_attribute"
  ],
  "schema_version": "1.0"
}
```
Response with the `schema_id` field, which you will need in the next step should look something like this. 
```json
{
    "schema_id": "{schema id from previous response}",
    "schema": {
        "ver": "1.0",
        "id": "A9A3zmbBnPT6RcrKvTf9q7:2:example_schema:1.0",
        "name": "example_schema",
        "version": "1.0",
        "attrNames": [
            "example_attribute"
        ],
        "seqNo": 23
    }
}
```
- Next we will create credential definition by request at `issuer_admin_url POST/credential-definitions` and paste this to the body.
- The value of `schema_id` field needs to replaced by the `schema_id` field of previous response.
```json
{
  "revocation_registry_size": 1000,
  "schema_id": "{schema_id}",
  "support_revocation": true,
  "tag": "default"
}
```
Now you should obtain a `credential_definition_id` which you also need to save for the next step. This is the response we get.
```json
{
    "credential_definition_id": "A9A3zmbBnPT6RcrKvTf9q7:3:CL:23:default"
}
```

## Sending offer to holder
- `connection_id` 
  - Use the `connection_id` from the first `create-invitation` request response.
- `credential_def_id` 
  - ` issuer_admin_url GET/credential-definitions/created` or the `credential_definition_id` from last response.
- Then replace the corresponding fields in the example body of `issuer_admin_url POST/issue-credential/send-offer` and also replace the attributes by schema `name` and `example_attribute`. 

The body should look something like this:
```json
{
  "auto_issue": "true",
  "auto_remove": "true",
  "comment": "string",
  "connection_id": "{connection_id}",
  "cred_def_id": "{cred_definition_id}",
  "credential_preview": {
      "@type": "issue-credential/1.0/credential-preview",
      "attributes": [
          {
              "name": "example_attribute",
              "value": "random_value"
          }
      ]
  },
  "trace": "true"
}
```
Now we just need to send request about this offer, from `holder_admin_url`, and we are done with issuing the credential!

## Holder sends a request
- We need to get `credential_exchange_id` from the response of `holder_admin_url GET/issue-credential/records`.
- Copy the `credential_exchange_id` field from the object which has the `thread_id` we obtained from the previous response. We will use it in the next step, if you for some reason do not find any existing record with the same `thread_id` try using `connection_id` for identifying current `credential_exchange_id`.
- Now we call send request in `holder_admin_url/ POST/issue-credential/records/{cred_ex_id}/send-request` with the `credential_exchange_id`.
### If everything went correct we have now created a credential for holder to present to verifier!
- Check created credentials by get request at this url:
    - `holder_admin_url GET/credentials`

# 2. Proof presentation

## Connection between Verifier and holder
- Now we need to again connect two agents but this time we will connect the verifier and holder of the created credential.
- Connect two agents by creating invitation at `verifier_admin_url POST/connections/create-invitation` the body of request should contain `{}`.
```json
{
    "connection_id": "570ac4b3-8b4f-453a-9d42-5dfdf019ddb2", // we will need this id later in tutorial
    "invitation": {
        "@type": "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation",
        "@id": "3290e401-fd9d-443f-9f72-3b68a81c6fce",
        "recipientKeys": [
            "EVs9yWghUMTjPT822qaZ58wQkt5ScGurdwZ6mNReSsdf"
        ],
        "serviceEndpoint": "http://localhost:8070",
        "label": "verifier"
    },
    "invitation_url": "http://localhost:8070?c_i=eyJAdHlwZSI6ICJkaWQ6c292OkJ6Q2JzTlloTXJqSGlxWkRUVUFTSGc7c3BlYy9jb25uZWN0aW9ucy8xLjAvaW52aXRhdGlvbiIsICJAaWQiOiAiMzI5MGU0MDEtZmQ5ZC00NDNmLTlmNzItM2I2OGE4MWM2ZmNlIiwgInJlY2lwaWVudEtleXMiOiBbIkVWczl5V2doVU1UalBUODIycWFaNTh3UWt0NVNjR3VyZHdaNm1OUmVTc2RmIl0sICJzZXJ2aWNlRW5kcG9pbnQiOiAiaHR0cDovL2xvY2FsaG9zdDo4MDcwIiwgImxhYmVsIjogInZlcmlmaWVyIn0="
}
```
- Save somewhere the `connection_id` from above we will use it later and paste the invitation field of the response to the holder into the  body of `holder_admin_url POST/connections/receive-invitation`.
```json
{
    "@type": "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation",
    "@id": "3290e401-fd9d-443f-9f72-3b68a81c6fce",
    "recipientKeys": [
        "EVs9yWghUMTjPT822qaZ58wQkt5ScGurdwZ6mNReSsdf"
    ],
    "serviceEndpoint": "http://localhost:8070",
    "label": "verifier"
}
```
- Now both agents should be connected successfully!
- In this case the connection was made between verifier and holder.


## Request Proof — Verifier to Prover
- The first step consists of sending a proof request from the verifier to the prover.
- Call `verifier_admin_url POST/present-proof/send-request`.
This is the body of the request: 
```json
{
  "comment": "string",
  "connection_id": "{connection_id of connection between verifier and prover from the `create-invitation` request response}",
  "proof_request": {
    "name": "Proof request",
    "non_revoked": {
    },
    "nonce": "1",
    "requested_attributes": {
      "additionalProp1": {
        "name": "example_attribute",
        "non_revoked": {}
      }
    },
    "requested_predicates": {},
    "version": "1.0"
  },
  "trace": false
}
```
## Proof presentation
- The prover may present the proof using the following call `holder_admin_url POST/present-proof/records/{pres_ex_id}/send-presentation`.
- The `presentation_exchange_id` may be found using this request `holder_admin_url GET/present-proof/records` and copy the `pres_ex_id` from the object which has the `thread_id` from the last response. 

- You need to call a `GET/credentials` to get the detailed credentials. 
- The value for the key named “referent”, that’s also known as the `credential_id` that will be used later on.
- Call a `holder_admin_url POST/present-proof/records/{pres_ex_id}/send-presentation` on the holder’s agent.

body:
```json
{
    "requested_attributes": {
        "additionalProp1": {
        "cred_id": "{the value of `referent` field in the response of GET/credentials}",
        "revealed":true
        }
    },
    "requested_predicates":{},
    "self_attested_attributes":{},
    "trace": false
}
```

## Verify the presentation
- Finally, the verifier verifies the presentation.
- We call `verifier_admin_url POST/present-proof/records/{pres_ex_id}/verify-presentation`.

## This is the end this tutorial, hope you had fun!