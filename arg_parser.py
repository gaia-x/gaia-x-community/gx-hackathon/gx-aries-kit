#
# Copyright 2021 dNation. All Rights Reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from argparse import ArgumentParser


def parser():
    parser = ArgumentParser()
    cmd_sp = parser.add_subparsers(dest="command")

    cmd_connect_p = cmd_sp.add_parser("connect")
    cmd_connect_p = cmd_connect_p.add_subparsers(dest="action")
    cmd_connect_p.add_parser("agents")

    cmd_did_p = cmd_sp.add_parser("did")
    cmd_did_sp = cmd_did_p.add_subparsers(dest="action")

    did_create_p = cmd_did_sp.add_parser("create")
    did_create_p.add_argument(
        "method", type=str, choices=["key", "sov"], help="DID method"
    )
    did_create_p.add_argument("key_type", type=str, help="key-type")

    did_resolve_p = cmd_did_sp.add_parser("resolve")
    did_resolve_p.add_argument("did", type=str, help="--did: help")

    cmd_vc_p = cmd_sp.add_parser("vc")
    cmd_vc_sp = cmd_vc_p.add_subparsers(dest="action")

    vc_issue_p = cmd_vc_sp.add_parser("issue")
    vc_issue_p.add_argument("issuer", type=str, help="--issuer:  <did>")
    vc_issue_p.add_argument("subject", type=str, help="--subject: <did>")

    cmd_vc_sp.add_parser("present")

    cmd_vc_sp.add_parser("verify")

    return parser
