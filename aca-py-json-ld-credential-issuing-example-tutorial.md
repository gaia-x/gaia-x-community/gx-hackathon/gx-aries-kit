# ACA-Py SSI issuing JSON-LD credential example tutorial

## Start a von network:
```bash
git clone https://github.com/bcgov/von-network
cd von-network
pip install -r server/requirements.txt
pip install python3-indy
./manage build
./manage start
```
### If you get this error
- `docker-compose: error while loading shared libraries: libz.so.1: failed to map segment from shared object`
- Do this:
    - `sudo mount /tmp -o remount,exec`
## Install indy tails server
```bash
git clone https://github.com/bcgov/indy-tails-server.git
./indy-tails-server/docker/manage up
```
Copy the url `url`, e.g. `https://{random code}.ngrok.io` from logs you will need it later.
```
docker logs docker_ngrok-tails-server_1
```
## Create public dids on the indy ledger
- Create public `did` for issuer and verifier in the ledger browser running on the port `9000` using two random seeds which we will use later.
- ### For example
    - Issuer:
      - 0923456ERFDZSXCVTYUO9986OREDFBBB
    - Holder:
      - Holder does not require a public did creation

# Start agents with docker
## Issuer/Verifier Agent creation
```bash
docker run --network=host bcgovimages/aries-cloudagent:py36-1.16-1_0.7.3 start \
    --label issuer \
    --auto-respond-messages \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label issuer.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --genesis-url http://localhost:9000/genesis \
    --wallet-type indy \
    -it http 0.0.0.0 8050 \
    -ot http \
    --admin 0.0.0.0 8051 \
    --admin-insecure-mode \
    --auto-ping-connection \
    --seed 0923456ERFDZSXCVTYUO9986OREDFBBB \
    -e http://localhost:8050 \
    --wallet-name issuer \
    --wallet-key issuer \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io} 
```

## Holder Agent creation
```bash
docker run --network=host bcgovimages/aries-cloudagent:py36-1.16-1_0.7.3 start \
    -e http://localhost:8090 \
    --label holder \
    --auto-ping-connection \
    --auto-respond-messages \
    --inbound-transport http 0.0.0.0 8090 \
    --outbound-transport http \
    --admin 0.0.0.0 8091 \
    --admin-insecure-mode \
    --wallet-type indy \
    --preserve-exchange-records \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label holder.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --auto-store-credential \
    --auto-respond-credential-offer \
    --debug-credentials \
    --genesis-url http://localhost:9000/genesis \
    --wallet-name holder \
    --wallet-key holder \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io}
```


# Start agents from source
## Install libindy
```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys CE7709D068DB5E88
sudo add-apt-repository "deb https://repo.sovrin.org/sdk/deb bionic master"
sudo apt-get update
sudo apt-get install -y libindy
```
## Install ACA-Py cli through pip
```bash
pip3 install aries-cloudagent
```
See if the installation was successful and also the available options:
```bash
aca-py start --help
```

## Install requirements for ACA-Py
- Install requirements for aca-py, from `requirements.txt` in this repository.

## Issuer/Verifier Agent creation
```bash
aca-py start \
    --label issuer \
    --auto-respond-messages \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label issuer.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --genesis-url http://localhost:9000/genesis \
    --wallet-type indy \
    -it http 0.0.0.0 8050 \
    -ot http \
    --admin 0.0.0.0 8051 \
    --admin-insecure-mode \
    --auto-ping-connection \
    --seed 0923456ERFDZSXCVTYUO9986OREDFBBB \
    -e http://localhost:8050 \
    --wallet-name issuer \
    --wallet-key issuer \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io} 
```

## Holder Agent creation
```bash
aca-py start \
    -e http://localhost:8090 \
    --label holder \
    --auto-ping-connection \
    --auto-respond-messages \
    --inbound-transport http 0.0.0.0 8090 \
    --outbound-transport http \
    --admin 0.0.0.0 8091 \
    --admin-insecure-mode \
    --wallet-type indy \
    --preserve-exchange-records \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label holder.Agent.trace \
    --auto-accept-invites \
    --auto-accept-requests \
    --auto-store-credential \
    --auto-respond-credential-offer \
    --debug-credentials \
    --genesis-url http://localhost:9000/genesis \
    --wallet-name holder \
    --wallet-key holder \
    --auto-provision \
    --tails-server-base-url {the copied url e.g. https://{random code}.ngrok.io}
```

# ACA-Py API GUI SSI establishment

**NOTE**  For communication with ACA-Py API, you can use tool like [PostMan](https://postman.com).

# Establish connection between Agents
- Connect two agents by creating invitation at `issuer_admin_url POST/connections/create-invitation` the body of request should contain `{}`.
```json
{
    "connection_id": "03ed149e-bb04-47f3-978a-34301d959f4e", // we will need this id later in tutorial
    "invitation": {
        "@type": "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation",
        "@id": "e8f32994-266c-406f-97e8-49d42c483f7c",
        "recipientKeys": [
            "6MGvTLzmFLbVvVx1t7ZqvDTXnPSumELsey5y1zPzNBQU"
        ],
        "serviceEndpoint": "http://localhost:8050",
        "label": "issuer"
    },
    "invitation_url": "http://localhost:8050?c_i=eyJAdHlwZSI6ICJkaWQ6c292OkJ6Q2JzTlloTXJqSGlxWkRUVUFTSGc7c3BlYy9jb25uZWN0aW9ucy8xLjAvaW52aXRhdGlvbiIsICJAaWQiOiAiZThmMzI5OTQtMjY2Yy00MDZmLTk3ZTgtNDlkNDJjNDgzZjdjIiwgInJlY2lwaWVudEtleXMiOiBbIjZNR3ZUTHptRkxiVnZWeDF0N1pxdkRUWG5QU3VtRUxzZXk1eTF6UHpOQlFVIl0sICJzZXJ2aWNlRW5kcG9pbnQiOiAiaHR0cDovL2xvY2FsaG9zdDo4MDUwIiwgImxhYmVsIjogImlzc3VlciJ9"
}
```
- Save somewhere the `connection_id` from above we will use it later and paste the `invitation` field of the response to the holder into the  body of `holder_admin_url POST/connections/receive-invitation`.
```json
{
    "@type": "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation",
    "@id": "e8f32994-266c-406f-97e8-49d42c483f7c",
    "recipientKeys": [
        "6MGvTLzmFLbVvVx1t7ZqvDTXnPSumELsey5y1zPzNBQU"
    ],
    "serviceEndpoint": "http://localhost:8050",
    "label": "issuer"
}
```
Now both agents should be connected successfully! In this case the connection was made between issuer and holder.

## Create a non-public did for all agents (verifier and holder)
- DID method ("key" or "sov").
- `key` type "ed25519" or "bls12381g2" (corresponding to signature types "Ed25519Signature2018" or "BbsBlsSignature2020").
- if you use DID method `sov` you must use key type "ed25519".
**Note** that "did:sov" must be a public DID (i.e. registered on the ledger) but "did:key" is not.

### To register the did use `issuer_admin_url/holder_admin_url POST/wallet/did/create`
- The body will look like this:
```json
{
  "method": "key",
  "options": {
    "key_type": "bls12381g2"
  }
}
```

- This is the response we get:
  - We will later use the `did` field.
```json
{
    "result": {
        "did": "did:key:zUC7DcjaWhYmWk3qZ8vgLxDQkjERH6ksgkdKGLik8mASwYFnrAHntNonwhR5eRjtx15gC5nJ9UKErdn3ezpTrqs2ooumWLcG2BzeQJjYS6114tmYnGNjJGxs6gueKUkXBSrjpBp",
        "verkey": "ynBhT1tYA8Bd9CFqB1XCTRsW2pSfen5PPx8veuheCetzSZLABiQe9FTQVSPeRhY33sNR74TRUkE5i86LhaXJyrP3hziA4YsA8f39XsN8awdgRN38DLnkvtVfrJ1dbthsKaW",
        "posture": "wallet_only",
        "key_type": "bls12381g2",
        "method": "key"
    }
}
```

## Issue JSON-LD credential
- Issuer can issue the credential to holder by this call `issuer_admin_url POST/issue-credential-2.0/send`.
- The body will contain created connection_id from the first step and both non-public dids.
- More specifically you will need to replace the `connection_id`, `issuer` key, `credentialSubject.id` and `proofType` with the generated values.
- In this tutorial we will use example attributes defined on schema.org. Although this is NOT RECOMMENDED (included here for illustrative purposes only) - individual attributes can't be validated (see the comment later on).
- Then you review the attributes and objects defined by `https://schema.org` and decide what you need to include in your credential.
- For example to issue a credential with `givenName`, `familyName` and `alumniOf` attributes, submit the following:
```json
{
    "connection_id": "{connection_id of connection between issuer and holder}",
    "filter": {
        "ld_proof": {
            "credential": {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1",
                    "https://w3id.org/citizenship/v1"
                ],
                "type": [
                    "VerifiableCredential",
                    "PermanentResident"
                ],
                "id": "https://credential.example.com/residents/1234567890",
                "issuer": "{non-public did of issuer}",
                "issuanceDate": "2020-01-01T12:00:00Z",
                "credentialSubject": {
                    "type": [
                        "PermanentResident"
                    ],
                    "id": "{non public did of holder}",
                    "givenName": "ALICE",
                    "familyName": "SMITH",
                    "gender": "Female",
                    "birthCountry": "Bahamas",
                    "birthDate": "1958-07-17"
                }
            },
            "options": {
                "proofType": "BbsBlsSignature2020"
            }
        }
    }
}
```

**Note** If you don't have `auto-respond-credential-offer` and `auto-store-credential` enabled, you will need to call `holder_admin_url /issue-credential-2.0/records/{cred_ex_id}/send-request` and `holder_admin_url /issue-credential-2.0/records/{cred_ex_id}/store` to finalize the credential issuing.
### To see the created credential use `holder_admin_url POST/credentials/w3c` with `{}` in body

# Proof presentation

## Request Proof — Verifier to Prover
- The first step consists of sending a proof request from the verifier to the prover or also called holder.
- Call `issuer_admin_url POST/present-proof-2.0/send-request`.
This is the body of the request: 

```json
{
    "comment": "string",
    "connection_id": "{connection_id}",
    "presentation_request": {
        "dif": {
            "options": {
                "challenge": "3fa85f64-5717-4562-b3fc-2c963f66afa7",
                "domain": "4jt78h47fh47"
            },
            "presentation_definition": {
                "id": "32f54163-7166-48f1-93d8-ff217bdb0654",
                "format": {
                    "ldp_vp": {
                        "proof_type": [
                            "BbsBlsSignature2020"
                        ]
                    }
                },
                "input_descriptors": [
                    {
                        "id": "citizenship_input_1",
                        "name": "EU Driver's License",
                        "schema": [
                            {
                                "uri": "https://www.w3.org/2018/credentials#VerifiableCredential"
                            },
                            {
                                "uri": "https://w3id.org/citizenship#PermanentResident"
                            }
                        ],
                        "constraints": {
                            "limit_disclosure": "required",
                            "is_holder": [
                                {
                                    "directive": "required",
                                    "field_id": [
                                        "1f44d55f-f161-4938-a659-f8026467f126"
                                    ]
                                }
                            ],
                            "fields": [
                                {
                                    "id": "1f44d55f-f161-4938-a659-f8026467f126",
                                    "path": [
                                        "$.credentialSubject.familyName"
                                    ],
                                    "purpose": "The claim must be from one of the specified issuers",
                                    "filter": {
                                        "const": "SMITH"
                                    }
                                },
                                {
                                    "path": [
                                        "$.credentialSubject.givenName"
                                    ],
                                    "purpose": "The claim must be from one of the specified issuers"
                                }
                            ]
                        }
                    }
                ]
            }
        }
    }
}
```

## Proof presentation
- Get the `pres_ex_id`:
  - Call `holder_admin_url GET/present-proof-2.0/records` and copy the `pres_ex_id` from the object which has the `thread_id` we obtained from the previous response. We will use it in the next step.
- The prover may present the proof using the following call `holder_admin_url POST/request-presentation-2.0/{pres_ex_id}/send-presentation`.
- We will use `pres_request` field from the previous response as the body of this request.
```json
{
  "dif": {
    "issuer_id": "did:key:zUC7Dus47jW5Avcne8LLsUvJSdwspmErgehxMWqZZy8eSSNoHZ4x8wgs77sAmQtCADED5RQP1WWhvt7KFNm6GGMxdSGpKu3PX6R9a61G9VoVsiFoRf1yoK6pzhq9jtFP3e2SmU9",
    "presentation_definition": {
      "format": {
        "ldp_vp": {
          "proof_type": [
            "BbsBlsSignature2020"
          ]
        }
      },
      "id": "32f54163-7166-48f1-93d8-ff217bdb0654",
      "input_descriptors": [
        {
          "id": "citizenship_input_1",
          "name": "Some kind of citizenship check",
          "schema": [
            {
              "uri": "https://www.w3.org/2018/credentials#VerifiableCredential"
            },
            {
              "uri": "https://w3id.org/citizenship#PermanentResident"
            }
          ],
          "constraints": {
            "limit_disclosure": "required",
            "is_holder": [
                {
                    "directive": "required",
                    "field_id": [
                        "1f44d55f-f161-4938-a659-f8026467f126",
                        "332be361-823a-4863-b18b-c3b930c5623e"
                    ]
                }
            ],
            "fields": [
              {
                "id": "1f44d55f-f161-4938-a659-f8026467f126",
                "path": [
                  "$.credentialSubject.familyName"
                ],
                "purpose": "The claim must be from one of the specified issuers",
                "filter": {
                  "const": "SMITH"
                }
              },
              {
                  "id": "332be361-823a-4863-b18b-c3b930c5623e",
                  "path": [
                      "$.id"
                  ],
                  "purpose": "Specify the id of the credential to present",
                  "filter": {
                      "const": "https://credential.example.com/residents/1234567890"
                  }
              }
            ]
          }
        }
      ]
    }
  }
}
```

## Verify the presentation
- Call `issuer_admin_url GET/present-proof/records` to obtain `pres_ex_id` from the object which has the `thread_id` we obtained from the previous response. We will use it in the next step.
- Finally, the verifier verifies the presentation.
- We call `issuer_admin_url POST/present-proof-2.0/records/{pres_ex_id}/verify-presentation`.

## This is the end this tutorial, hope you had fun!
