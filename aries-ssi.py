#
# Copyright 2021 dNation. All Rights Reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import requests
import time
from arg_parser import parser

ISSUER_ADMIN_URL = "http://localhost:8051"
HOLDER_ADMIN_URL = "http://localhost:8091"


def offer_template(con_id, iss_did, creds_did):
    return {
        "connection_id": con_id,
        "filter": {
            "ld_proof": {
                "credential": {
                    "@context": [
                        "https://www.w3.org/2018/credentials/v1",
                        "https://w3id.org/citizenship/v1",
                    ],
                    "type": ["VerifiableCredential", "PermanentResident"],
                    "id": "https://credential.example.com/residents/1234567890",
                    "issuer": iss_did,
                    "issuanceDate": "2020-01-01T12:00:00Z",
                    "credentialSubject": {
                        "type": ["PermanentResident"],
                        "id": creds_did,
                        "givenName": "ALICE",
                        "familyName": "SMITH",
                        "gender": "Female",
                        "birthCountry": "Bahamas",
                        "birthDate": "1958-07-17",
                    },
                },
                "options": {"proofType": "BbsBlsSignature2020"},
            }
        },
    }


def presentation_request_template(con_id):
    return {
        "comment": "string",
        "connection_id": con_id,
        "presentation_request": {
            "dif": {
                "options": {
                    "challenge": "3fa85f64-5717-4562-b3fc-2c963f66afa7",
                    "domain": "4jt78h47fh47",
                },
                "presentation_definition": {
                    "id": "32f54163-7166-48f1-93d8-ff217bdb0654",
                    "format": {"ldp_vp": {"proof_type": ["BbsBlsSignature2020"]}},
                    "input_descriptors": [
                        {
                            "id": "citizenship_input_1",
                            "name": "EU Driver's License",
                            "schema": [
                                {
                                    "uri": "https://www.w3.org/2018/credentials#VerifiableCredential"
                                },
                                {
                                    "uri": "https://w3id.org/citizenship#PermanentResident"
                                },
                            ],
                            "constraints": {
                                "limit_disclosure": "required",
                                "is_holder": [
                                    {
                                        "directive": "required",
                                        "field_id": [
                                            "1f44d55f-f161-4938-a659-f8026467f126"
                                        ],
                                    }
                                ],
                                "fields": [
                                    {
                                        "id": "1f44d55f-f161-4938-a659-f8026467f126",
                                        "path": ["$.credentialSubject.familyName"],
                                        "purpose": "The claim must be from one of the specified issuers",
                                        "filter": {"const": "SMITH"},
                                    },
                                    {
                                        "path": ["$.credentialSubject.givenName"],
                                        "purpose": "The claim must be from one of the specified issuers",
                                    },
                                ],
                            },
                        }
                    ],
                },
            }
        },
    }


def get_pres_ex_id(records, th_id):
    for r in records:
        if r["thread_id"] == th_id:
            return r["pres_ex_id"]


def send_request(url, data=None, post=True):
    retries = 0
    max_retries = 5
    time.sleep(1)
    while retries < max_retries:
        try:
            if post:
                response = requests.post(url, json=data)
            else:
                response = requests.get(url, json=data)
            if response.status_code == requests.codes.ok:
                return response
            else:
                time.sleep(2 ** retries)
                retries += 1
                if retries >= max_retries:
                    return response
        except requests.exceptions.RequestException as e:
            time.sleep(2 ** retries)
            retries += 1
            if retries >= max_retries:
                raise e


def connect_agents():
    resp = send_request(ISSUER_ADMIN_URL + "/connections/create-invitation", {})
    invitation = resp.json()["invitation"]
    con_id = resp.json()["connection_id"]
    send_request(HOLDER_ADMIN_URL + "/connections/receive-invitation", invitation)
    print(
        "Connection between the issuer and holder successful!\n"
        f"Connection ID: {con_id}"
    )
    return con_id


def did_create(args_):
    key_def = {"method": args_.method, "options": {"key_type": args_.key_type}}
    cred_subj_did = send_request(
        HOLDER_ADMIN_URL + "/wallet/did/create", key_def
    ).json()["result"]["did"]
    issuer_id = send_request(ISSUER_ADMIN_URL + "/wallet/did/create", key_def).json()[
        "result"
    ]["did"]
    print(f"Issuer DID: {issuer_id}\n" f"Credential subject DID: {cred_subj_did}")


def did_resolve(args_):
    cred_did_resolve = send_request(
        HOLDER_ADMIN_URL + f"/resolver/resolve/{args_.did}", post=False
    ).json()["did_document"]
    iss_did_resolve = send_request(
        ISSUER_ADMIN_URL + f"/resolver/resolve/{args_.did}", post=False
    ).json()["did_document"]
    if cred_did_resolve:
        print(cred_did_resolve)
    else:
        print(iss_did_resolve)


def vc_issue(args_):
    try:
        con_id = send_request(ISSUER_ADMIN_URL + "/connections", post=False).json()[
            "results"
        ][-1]["connection_id"]
    except IndexError:
        con_id = connect_agents()
    offer_data = offer_template(con_id, args_.issuer, args_.subject)
    sent = send_request(ISSUER_ADMIN_URL + "/issue-credential-2.0/send", offer_data)
    issued_cred = sent.json()["by_format"]["cred_proposal"]["ld_proof"]["credential"]
    print(issued_cred)


def present_proof():
    try:
        con_id = send_request(ISSUER_ADMIN_URL + "/connections", post=False).json()[
            "results"
        ][-1]["connection_id"]
    except IndexError:
        con_id = connect_agents()
    pp_request = presentation_request_template(con_id)
    presentation_res = send_request(
        ISSUER_ADMIN_URL + "/present-proof-2.0/send-request", pp_request
    ).json()
    presentation_body = presentation_res["by_format"]["pres_request"]
    th_id = presentation_res["thread_id"]
    records = send_request(
        HOLDER_ADMIN_URL + "/present-proof-2.0/records", post=False
    ).json()["results"]
    pres_ex_id = get_pres_ex_id(records, th_id)
    sent = send_request(
        HOLDER_ADMIN_URL + f"/present-proof-2.0/records/{pres_ex_id}/send-presentation",
        presentation_body,
    )
    thread_id = sent.json()["thread_id"]
    presentation_body = sent.json()["pres"]["presentations~attach"][0]["data"]["json"]
    return thread_id, presentation_body


def verify_proof():
    th_id, pres_body = present_proof()
    records = send_request(
        ISSUER_ADMIN_URL + "/present-proof-2.0/records", post=False
    ).json()["results"]
    pres_ex_id = get_pres_ex_id(records, th_id)
    verify_presentation_response = requests.post(
        ISSUER_ADMIN_URL
        + f"/present-proof-2.0/records/{pres_ex_id}/verify-presentation"
    )
    verify_presentation = verify_presentation_response.json()["verified"]
    return verify_presentation


if __name__ == "__main__":
    args = parser().parse_args()
    if args.command == "connect" and args.action == "agents":
        connect_agents()
    if args.command == "did" and args.action == "create":
        did_create(args)
    if args.command == "did" and args.action == "resolve":
        did_resolve(args)
    if args.command == "vc" and args.action == "issue":
        vc_issue(args)
    if args.command == "vc" and args.action == "present":
        thread_id, presentation_body = present_proof()
        print(presentation_body)
    if args.command == "vc" and args.action == "verify":
        verified = verify_proof()
        print(f"Verified: {verified}")
